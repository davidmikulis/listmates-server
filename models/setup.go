package models

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// DB is the connection to the database
var DB *gorm.DB

func setup(db *gorm.DB) {
	db.AutoMigrate(&List{})
	seed(db)
}

func seed(db *gorm.DB) {

}

// ConnectDatabase opens a connection to the database
func ConnectDatabase() {
	database, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		panic(err)
	}

	database.AutoMigrate(&List{})

	DB = database
}
