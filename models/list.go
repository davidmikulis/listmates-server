package models

import (
	"gorm.io/gorm"
)

// List is the database model of a list
type List struct {
	gorm.Model
	Name        string `json:"name"`
	Description string `json:"description"`
}

// FindList returns a List from the database by ID
func FindList(list *List, id int) error {
	err := DB.First(list, id).Error
	return err
}

// FindAllLists returns all lists from the database
func FindAllLists(lists *[]List) error {
	return DB.Find(lists).Error
}

// CreateList creates a new List entry in the database
func CreateList(list *List) error {
	return DB.Create(&list).Error
}

// DeleteList deletes the List entry from the database
func DeleteList(id int) error {
	return DB.Delete(&List{}, id).Error
}
