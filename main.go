package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/davidmikulis/listmates-server/controllers"
	"gitlab.com/davidmikulis/listmates-server/models"
)

func main() {
	engine := gin.Default()

	engine.GET("/lists", controllers.GetLists)
	engine.POST("/lists", controllers.PostList)
	engine.GET("/lists/:id", controllers.GetList)
	// engine.PATCH("/lists/:id", controllers.UpdateList)
	// engine.DELETE("/lists/:id", controllers.DeleteList)

	models.ConnectDatabase()

	engine.Run(":8080")
}
