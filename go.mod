module gitlab.com/davidmikulis/listmates-server

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.9
)
