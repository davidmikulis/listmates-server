package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/davidmikulis/listmates-server/models"
	"gorm.io/gorm"
)

// CreateListInput is a struct for the JSON POST body to create a list
type CreateListInput struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
}

// GetLists returns all lists
func GetLists(context *gin.Context) {
	var lists []models.List
	if err := models.FindAllLists(&lists); err != nil {
		SendErr(context, http.StatusInternalServerError, err.Error())
		return
	}

	SendData(context, lists)
}

// GetList returns a list based on the passed ID parameter
func GetList(context *gin.Context) {
	var params ParamsWithID
	if err := context.ShouldBindUri(&params); err != nil {
		fmt.Println(err.Error())
		context.AbortWithStatus(400)
		return
	}

	var list models.List
	if err := models.FindList(&list, params.ID); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			SendErr(context, http.StatusNotFound, err.Error())
			return
		}
		SendErr(context, http.StatusInternalServerError, err.Error())
		return
	}

	SendData(context, list)
}

// PostList creates a new list and returns it
func PostList(context *gin.Context) {
	var input CreateListInput
	if err := context.BindJSON(&input); err != nil {
		SendErr(context, http.StatusBadRequest, err.Error())
		return
	}

	list := models.List{Name: input.Name, Description: input.Description}
	models.CreateList(&list)

}

// DeleteList deletes a list from the database
func DeleteList(context *gin.Context) {
	var id int
	var err error
	if id, err = strconv.Atoi(context.Param("id")); err != nil {
		SendErr(context, http.StatusBadRequest, err.Error())
		return
	}

	if err := models.DeleteList(id); err != nil {
		SendErr(context, http.StatusNotFound, err.Error())
		return
	}

}
