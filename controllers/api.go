package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// APIResponse is a generic struct for API data returns
type APIResponse struct {
	Error *string     `json:"error"`
	Data  interface{} `json:"data"`
}

// ParamsWithID is used by ShouldBindUri to confirm ID param is valid
type ParamsWithID struct {
	ID int `uri:"id" binding:"required"`
}

// SendData returns valid API data to the client
func SendData(context *gin.Context, data interface{}) {
	context.JSON(http.StatusOK, APIResponse{Data: data, Error: nil})
}

// SendErr returns the specified error code and error message
func SendErr(context *gin.Context, code int, err string) {
	context.JSON(code, APIResponse{Error: &err, Data: nil})
}
